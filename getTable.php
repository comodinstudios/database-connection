<?php
include 'config.php';

if($_POST){
	$limit = urldecode($_POST['Limit']);
	$table = urldecode($_POST['TableName']);
	if(is_null($limit) OR empty($limit) OR is_null($table) OR empty($table))
		die("Need limit");
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	$sql = "SELECT * FROM ".$table." LIMIT ".$limit;
	$result = mysqli_query($conn, $sql);
	
	$columns = array();
	while($column = mysqli_fetch_field($result)){
		array_push($columns, $column);
	}
	
	$rows = array();
	while($row = mysqli_fetch_array($result)){
		for($i = 0; $i < mysqli_num_fields($result); $i++){
			$row_obj[$columns[$i]->name] = utf8_encode($row[$i]);
		}
		array_push($rows, $row_obj);
	}
	echo json_encode($rows);
}
else
	echo 'Access Denied.';
?>