<?php
include 'config.php';
header("Content-Type: application/json");

if($_POST)
{
    $table = urldecode($_POST['TableName']);
    if(is_null($table) OR empty($table))
		die("Login or Password is null or empty");
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	$sql = "CHECKSUM TABLE ".$table;
	$result = json_encode(mysqli_fetch_array(mysqli_query($conn, $sql)));
	echo md5($result);
}
else
	echo "Access Denied.";
?>