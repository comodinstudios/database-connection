<?php
include 'config.php';

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM Product";
$result = mysqli_query($conn, $sql);
if ($result->num_rows > 0){
	echo ($result->num_rows)." results";
	$emparray = array();
	while($row = mysqli_fetch_array($result)){
		array_push($emparray, $row);
	}
	$json_data = json_encode($emparray);
	echo $json_data;
}
else {
	echo "0 results";
}
?>