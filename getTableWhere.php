<?php
include 'config.php';

if($_POST){
	$id = urldecode($_POST['PrimaryKeyValue']);
	$field = urldecode($_POST['PrimaryKeyField']);
	$table = urldecode($_POST['TableName']);
	if(is_null($id) OR empty($id) OR is_null($field) OR empty($field) OR is_null($table) OR empty($table))
		die("Need limit");
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	$sql = "SELECT * FROM ".$table." WHERE ".$field."='".$id."'";
	$result = mysqli_query($conn, $sql);
	
	$columns = array();
	while($column = mysqli_fetch_field($result)){
		array_push($columns, $column);
	}
	
	$rows = array();
	while($row = mysqli_fetch_array($result)){
		for($i = 0; $i < mysqli_num_fields($result); $i++){
			$row_obj[$columns[$i]->name] = utf8_encode($row[$i]);
		}
		array_push($rows, $row_obj);
	}
	echo json_encode($rows);
}
else
	echo 'Access Denied.';
?>